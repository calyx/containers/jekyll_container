FROM debian:bullseye-slim

# exclude unnecessary files from being installed

COPY scripts/templates/nodoc /etc/dpkg/dpkg.cfg.d/nodoc

# install packages needed to build gems, build gems, then remove unneeded build packages

RUN \
  apt-get update \
  && apt-get install -y \
    build-essential \
    git \
    inotify-tools \
    ruby \
    ruby-dev \
    zlib1g-dev \
  && rm -rf /var/lib/apt/lists/* \
  && gem install --no-document --bindir /usr/bin \
    bundler \
    ffi \
    i18n \
    jekyll \
    nokogiri \
    racc \
    sassc \
  && rm -rf /usr/share/ri \
  # if you want a much smaller image, you can remove the dev packages, but then
  # it limits what gems can be installed:
  #&& apt-get remove -y build-essential \
  #&& apt-get remove -y $(dpkg --get-selections | grep '\-dev' | cut -f1) \
  && apt-get autoremove -y

# forward logs to stdout & stderr

VOLUME /srv

STOPSIGNAL SIGQUIT

COPY scripts /usr/local/bin/

ENTRYPOINT ["start"]