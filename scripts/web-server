#!/usr/bin/env ruby
#
# a very minimal web server to serve static files with support for virtual domains
#
require_relative 'common'
require "webrick"
require "net/http"

def main
  $sites = parse_env
  server = WEBrick::HTTPServer.new(:Port => ENV["WEB_PORT"] || 8080)
  server.mount "/", StaticServer
  trap("INT"){ server.shutdown }
  server.start
end

class StaticServer < WEBrick::HTTPServlet::DefaultFileHandler
  def initialize(server)
    super(server, '/tmp/dummy')
  end

  def do_GET(request, response)
    root = document_root(request.host)
    if root
      @local_path = File.join(root, request.path)
      if File.directory?(@local_path)
        @local_path = File.join(@local_path, 'index.html')
      elsif File.exist?("#{@local_path}.html")
        @local_path = "#{@local_path}.html"
      end
      if !File.exist?(@local_path)
        render_not_found(request, response, root)
        return
      end
    else
      @local_path = nil
    end
    super
  end

  private

  def render_not_found(request, response, doc_root)
    not_found_page_path = File.join(doc_root, '404.html')
    response.status = 404
    response.content_type = 'text/html'
    if File.exist?(not_found_page_path)
      response.body = File.read(not_found_page_path)
    else
      response.body = NOT_FOUND_CONTENT
    end
  end

  def document_root(domain)
    site_name = get_site_name_from_domain(domain)
    if site_name
      File.join(SERVICE_DIR, site_name, $sites[site_name]["root"])
    else
      nil
    end
  end

  def get_site_name_from_domain(domain)
    $sites.each do |name, site|
      if name == domain || site["domains"].include?(domain)
        return name
      end
    end
    return nil
  end

  NOT_FOUND_CONTENT=\
%[<html>
  <head>
    <title>Not Found</title>
    <style>
      html {
        font-weight: bold;
        font-family: sans-serif;
      }
      body {
        display: flex;
        overflow: hidden;
        position: absolute;
        width: 100%;
        height: 100%;
        flex-direction: column;
        align-items: center;
        justify-content: center;
      }
      span {
        color: #888;
      }
    </style>
  </head>
  <body>
    <h1>404 <span>Not Found</span></h1>
    <p>The page you requested could not be found.</p>
  </body>
</html>]

end

main()