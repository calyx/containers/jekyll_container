#!/usr/bin/env ruby
#
# setup entry point for the container
#
require_relative 'common'

Signal.trap("INT") { output("DONE"); exit }
Signal.trap("TERM") { output("DONE"); exit }
Signal.trap("QUIT") { output("DONE"); exit }

threads = []

##
## SETUP
##

setup
sites = parse_env(verbose: true)
sites.each do |name, site|
  git_clone(name, site, prefix: "SETUP > GIT")
  bundle_install(name, site, prefix: "SETUP > BUNDLE")
  jekyll_build(name, site, prefix: "SETUP > JEKYLL")
end

##
## WEB SERVER
##

output "WEB: Starting..."
threads << Thread.new {
  run WEB_SERVER, prefix: "WEB"
}

##
## GIT POLLER
##

output "POLLER: Starting..."
threads << Thread.new {
  run GIT_POLLER, prefix: "POLLER"
}

##
## WATCHERS
##

sites.each do |name, site|
  threads << Thread.new {
    run SITE_WATCHER, File.join(SERVICE_DIR, name), prefix: "WATCHER #{name}"
  }
end

##
## END
##

threads.map(&:join)
output "DONE"
