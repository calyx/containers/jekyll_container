A docker container to build & deploy multiple static Jekyll sites from git.

Features:

* 100% configured from environment variables.
* Support as many domains and as many jekyll sites as you want.
* Just specify domains and git repository, and everything else is taken care of.
* Poll for git updates every 5 minutes.

Limitations:

* Only works with public git repositories.
* No https: this is intended to be run behind a proxy that provides TLS.
* The shared gem cache `/srv/gems` might fill up with unwanted gems if the same volume is used over a long period of time.

Prequisites
===============================

Install docker

    # apt install docker.io
    # usermod -aG docker $USER
    $ newgrp docker

Make sure dockerd is running, start if not:

    # systemctl status docker
    # systemctl start docker

Run hello world:

    $ docker run hello-world

You probably don't want to run docker on boot:

    # systemctl disable docker

Build image
===============================

    $ cd build
    $ docker build -t static .

From time to time the debian base image will update to reflect new packages. To force an update that incorporates these changes to the base debian image:

    $ docker build --no-cache -t static .

Run container
===============================

Command switches:

* `-p LOCAL_PORT:8080`     -- map port 8080 in container to LOCAL_PORT on the host
* `-v /tmp/srv:/srv`       -- allow persistant storage at /srv (required)
* `-e SITE_0=<domain>`     -- define sites to host (required)
* `-e SITE_0_GIT=<domain>` -- define sites to host (required)
* `-e WEB_PORT=8080`       -- what port the in container web server will listen on (default 8080)
* `-n static`              -- optional name
* `-it`                    -- optional, useful for testing, so you can hit ^C to kill the container.

For example:

    $ docker run -p 4000:8080 -v /tmp/srv:/srv -e SITE_0=localhost -e SITE_0_GIT="https://0xacab.org/calyx/websites/calyx.net.git" -it static

Read only:

```
docker run \
  --security-opt=no-new-privileges \
  --cap-drop=all \
  --read-only \
  -p 4000:8080 \
  -v /tmp/srv:/srv \
  -e SITE_0=localhost \
  -e SITE_0_GIT="https://0xacab.org/calyx/websites/calyx.net.git" \
  static
```

Configuration
===============================

SITES
-------------------------------

The environent variable `SITE_X` is used to build the static jekyll pages and nginx configuration. For example:

    SITE_0=example.org
    SITE_0_GIT=https://github.com/example/example.org.git
    SITE_0_DOMAINS=www.example.org,example.com
    SITE_0_BUILD="bundle exec jekyll build"

You can have as many sites configured as you want. Each one is numbered, just replace 0 with the site number. The processing stops when it hits an empty variable (e.g. it will not read SITE_10 if SITE_9 is undefined).
